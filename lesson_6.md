# RDS

Add an RDS DB to terraform.

Create it by hand first? Maybe to see the settings, but maybe it's not
worth it.

Add autoscaling rules for server nodes. See what this does to our performance.

Maybe more about persistence here. Do we want the bashcgi101 lesson to
have them introduce persistence. Then slam with tests and see how it creates
hot spots which effect performance? Then once they make the app use
a shared back end they can disable persistance and see how the autoscaling
rules now let us spin up more nodes dramatically increasing performance.
