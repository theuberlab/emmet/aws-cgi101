# AWS and Terraform

A companion to [bashcgi101](https://gitlab.com/theuberlab/emmet/bashcgi101)  
where we will learn how to create servers, load balancers and dbs in AWS.

They will be presented as lessons which you can follow along with and learn.

But they will also provide directions and scripts to set up the environment
for the lessons in bashcgi101 so you can simply run the scripts then
go back to the beginning to learn AWS.

